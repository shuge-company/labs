#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int outChar = 0, counter = 128;
pid_t pid;

void sigchldHandler(int signo)
{
    exit(EXIT_SUCCESS);
}

void sigalrmHandler(int signo)
{
    exit(EXIT_SUCCESS);
}

void empty(int signo){}

void odd(int signo)
{
    // printf("outchar odd %d   counter = %d\n", outChar, counter);
    outChar += counter;
    counter /=2;
    kill(pid,SIGUSR1);
}

void even(int signo)
{
    // printf("outchar odd %d   counter = %d\n", outChar, counter);
    counter /=2;
    kill(pid,SIGUSR1);
}

void childFunction(sigset_t& set, char* fileName, pid_t ppid)
{

}


int main(int argc, char ** argv)
{
    if(argc != 2)
    {
        printf("add source");
        exit(EXIT_FAILURE);
    }

    pid_t ppid = getpid();
    
    sigset_t set;

    struct sigaction act_exit;
    memset(&act_exit, 0, sizeof(act_exit));
    act_exit.sa_handler = sigchldHandler;
    sigfillset(&act_exit.sa_mask);
    sigaction(SIGCHLD, &act_exit,NULL);

    struct sigaction act_odd;
    memset(&act_odd, 0, sizeof(act_odd));
    act_odd.sa_handler = odd;
    sigfillset(&act_odd.sa_mask);
    sigaction(SIGUSR1, &act_odd, NULL);

    struct sigaction act_even;
    memset(&act_odd, 0, sizeof(act_even));
    act_even.sa_handler = even;
    sigfillset(&act_even.sa_mask);
    sigaction(SIGUSR2, &act_even, NULL);


    sigaddset(&set, SIGUSR1);
    sigaddset(&set, SIGUSR2);
    sigaddset(&set, SIGCHLD);
    sigprocmask(SIG_BLOCK,&set,NULL);
    sigemptyset(&set);


    pid = fork();

    if(pid == 0)
    {
            unsigned int fd = 0;
        char c = 0;
        sigemptyset(&set);

        struct sigaction act_empty;
        memset(&act_empty,0,sizeof(act_empty));
        act_empty.sa_handler = empty;
        sigfillset(&act_empty.sa_mask);
        sigaction(SIGALRM, &act_empty,NULL);

        struct sigaction act_alarm;
        memset(&act_alarm, 0, sizeof(act_alarm));
        act_alarm.sa_handler = sigalrmHandler;
        sigfillset(&act_alarm.sa_mask);
        sigaction(SIGALRM,&act_alarm,NULL);
        if((fd = open(argv[1], O_RDONLY)) < 0)
        {
            perror("cant open file");
            exit(EXIT_FAILURE);
        }
        int i;
        while(read(fd,&c,1)>0)
        {
            alarm(1);
            //printf("c = %d ", c);
            for(i = 128; i >=1; i/=2)
            {

                if(i&c)
                    kill(ppid,SIGUSR1);
                else
                    kill(ppid,SIGUSR2);
                sigsuspend(&set);
            }
        }
        exit(EXIT_SUCCESS);
    }
    
    do
    {
        //printf("%d", counter);
        if(counter == 0){
            write(STDOUT_FILENO,&outChar,1);
            //printf("outchar = %d\n", outChar);
            fflush(stdout);
            counter = 128;
            outChar = 0;
        }
        sigsuspend(&set);
    } while(1);

    exit(EXIT_SUCCESS);



}