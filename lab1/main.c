#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>


void reader(const char *fifoName)
{
    int rd;
    rd = open(fifoName,O_RDONLY);
    int len;
    char* buf = (char*) malloc(256);
    while(len = read(rd,buf,256) > 0)
    {
        printf("%s", buf);
        memset(buf,'\0',256);
    }
    close(rd);
}

void writer(const char *fifoName, const char *fileName)
{
    int wr, fd;
    wr = open(fifoName,O_WRONLY);
    fd = open(fileName, O_RDONLY);

    int len;
    char *buf = (char *) malloc(256);
    while( (len = read(fd,buf,256)) > 0)
    {
        if(write(wr,buf,len) <=0)
        {
            printf("error while write");
            exit(3);
        }
    }

}

int main(int argc, char **argv)
{
    if(argc <2)
    {
        printf("write filename to read");
        exit(1);
    }
    const char *fifoName = "myfifo";
    if(mkfifo(fifoName,S_IRWXU) != 0)
    {
        printf("Cant create fifo errno = %d\n", errno);
        exit(1);
    }
    int status;
    pid_t pid;
    if( (pid = fork()) < 0)
    {
        printf("cant fork\n");

        exit(2);
    }    

    if(pid == (pid_t)0)
    {
        writer(fifoName, argv[1]);
        exit(0);
    }
    else
    {
        reader(fifoName);
        waitpid(-1,&status,0);
    }

    unlink(fifoName);
    printf("\n\n\nprogram end\n");
    return 0;
}